package midi

func toVariableLength(input int) []byte {
	numBytes := variableLengthSize(input)
	result := make([]byte, numBytes)

	for i := range result {
		shifted := input >> (7 * uint(len(result)-1-i))
		result[i] = byte(shifted & 0x7F)

		if i != len(result)-1 {
			result[i] = result[i] | 0x80
		}
	}

	return result
}

func variableLengthSize(input int) int {
	result := 1

	for input > 0x7F {
		input = input >> 7
		result++
	}

	return result
}
