# midi

The midi package implements a simple library for creating MIDI (.mid) files.  It is not intended to be a complete standard-compliant package, therefore it only supports a limited number of features.

The supported features are:

* custom time division (only in ticks per beat)
* Note-ON and Note-OFF events

### Usage

The following example writes a quarter note on A4 to a MIDI file.

    m := new(midi.Midi)
    m.Events = []midi.Event{
        midi.NoteOnEvent{0, 81, 127},
        midi.NoteOffEvent{4, 81, 127},
    }
    
    err := m.Encode(someFile)