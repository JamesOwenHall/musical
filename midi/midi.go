// Package midi implements a simple library for creating MIDI files.  See README.md for details.
package midi

import (
	"bytes"
	"encoding/binary"
	"io"
)

var (
	TicksPerBeat int16 = 4
)

type Midi struct {
	Events []Event
}

// Encode writes the binary representation of the midi file to an io.Writer
func (m *Midi) Encode(w io.Writer) error {
	var err error

	err = m.writeHeader(w)
	if err != nil {
		return err
	}

	err = m.writeTrack(w)

	return err
}

func (m *Midi) writeHeader(w io.Writer) error {
	var err error
	buf := new(bytes.Buffer)

	// header ID, always "MThd"

	_, err = buf.WriteString("MThd")
	if err != nil {
		return err
	}

	// header chunk size, always 6

	_, err = buf.Write([]byte{0x00, 0x00, 0x00, 0x06})
	if err != nil {
		return err
	}

	// format type, only type 0 is supported

	_, err = buf.Write([]byte{0x00, 0x00})
	if err != nil {
		return err
	}

	// number of tracks, always 1 for format type 0

	_, err = buf.Write([]byte{0x00, 0x01})
	if err != nil {
		return err
	}

	// time division in ticks per beat (grid spaces per quarter note)

	err = binary.Write(buf, binary.BigEndian, TicksPerBeat)
	if err != nil {
		return err
	}

	_, err = w.Write(buf.Bytes())

	return err
}

func (m *Midi) writeTrack(w io.Writer) error {
	var err error
	buf := new(bytes.Buffer)

	// track ID, always "MTrk"

	_, err = buf.WriteString("MTrk")
	if err != nil {
		return err
	}

	// track size

	err = binary.Write(buf, binary.BigEndian, int32(m.trackSize()))
	if err != nil {
		return err
	}

	// events

	for _, e := range m.Events {
		_, err = buf.Write(e.bytes())
		if err != nil {
			return err
		}
	}

	_, err = w.Write(buf.Bytes())

	return err
}

func (m *Midi) trackSize() int {
	result := 0

	for _, e := range m.Events {
		result += e.len()
	}

	return result
}
