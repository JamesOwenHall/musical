package midi

import (
	"testing"
)

func TestToVariableLength(t *testing.T) {
	var in int
	var exp, out []byte

	in = 0x00
	exp = []byte{0x00}
	if out = toVariableLength(in); !equalByteSlice(out, exp) {
		t.Errorf("Input: %d; Expected: %d; Out: %d", in, exp, out)
	}

	in = 0xC8
	exp = []byte{0x81, 0x48}
	if out = toVariableLength(in); !equalByteSlice(out, exp) {
		t.Errorf("Input: %d; Expected: %d; Out: %d", in, exp, out)
	}

	in = 0x100000
	exp = []byte{0xC0, 0x80, 0x00}
	if out = toVariableLength(in); !equalByteSlice(out, exp) {
		t.Errorf("Input: %d; Expected: %d; Out: %d", in, exp, out)
	}
}

func equalByteSlice(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}
