package midi

type Event interface {
	bytes() []byte
	len() int
}

type NoteOnEvent struct {
	DeltaTime  int
	NoteNumber byte
	Velocity   byte
}

func (n NoteOnEvent) bytes() []byte {
	result := make([]byte, 0, n.len())

	deltaBytes := toVariableLength(n.DeltaTime)

	for _, b := range deltaBytes {
		result = append(result, b)
	}

	result = append(result, 0x90, n.NoteNumber, n.Velocity)

	return result
}

func (n NoteOnEvent) len() int {
	return 3 + variableLengthSize(n.DeltaTime)
}

type NoteOffEvent struct {
	DeltaTime  int
	NoteNumber byte
	Velocity   byte
}

func (n NoteOffEvent) bytes() []byte {
	result := make([]byte, 0, n.len())

	deltaBytes := toVariableLength(n.DeltaTime)

	for _, b := range deltaBytes {
		result = append(result, b)
	}

	result = append(result, 0x80, n.NoteNumber, n.Velocity)

	return result
}

func (n NoteOffEvent) len() int {
	return 3 + variableLengthSize(n.DeltaTime)
}
