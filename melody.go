package main

import (
	"container/list"
	"github.com/JamesOwenHall/musical/midi"
	"io"
)

// Note allows the representation of a note press and release in terms of its
// start time and duration instead of its start and end times.
type Note struct {
	Start      float64
	Duration   float64
	NoteNumber byte
	Velocity   byte
}

// Melody provides an easy to use structure based on Notes instead of events.
type Melody struct {
	Notes []Note
}

// ExportMidi writes the binary MIDI representation of the melody.
func (m *Melody) ExportMidi(w io.Writer) error {
	sequence := new(orderedNoteSequence)

	for _, n := range m.Notes {
		sequence.addNote(n)
	}

	mid := new(midi.Midi)
	mid.Events = sequence.Events()

	return mid.Encode(w)
}

// orderedNoteSequence is a wrapper for a list that keeps a sequence of
// noteEvents in order with respect to their time.
type orderedNoteSequence struct {
	list *list.List
}

func (o *orderedNoteSequence) addNote(n Note) {
	if o.list == nil {
		o.list = list.New()
	}

	start := noteEvent{n.Start, noteStart, n}
	end := noteEvent{n.Start + n.Duration, noteEnd, n}

	o.insertNoteEvent(start)
	o.insertNoteEvent(end)
}

func (o *orderedNoteSequence) insertNoteEvent(e noteEvent) {
	iterator := o.list.Front()

	for iterator != nil && iterator.Value.(noteEvent).time <= e.time {
		iterator = iterator.Next()
	}

	if iterator == nil {
		o.list.PushBack(e)
	} else {
		o.list.InsertBefore(e, iterator)
	}
}

// Events converts the list of noteEvents into midi.Event instances.
func (o *orderedNoteSequence) Events() []midi.Event {
	result := make([]midi.Event, 0, o.list.Len())

	for i := o.list.Front(); i != nil; i = i.Next() {
		ev := i.Value.(noteEvent)

		var deltaTime int

		if prev := i.Prev(); prev == nil {
			deltaTime = int(ev.time * float64(midi.TicksPerBeat))
		} else {
			prevTime := prev.Value.(noteEvent).time
			deltaTime = int((ev.time - prevTime) * float64(midi.TicksPerBeat))
		}

		switch ev.kind {
		case noteStart:
			result = append(result, midi.NoteOnEvent{deltaTime, ev.note.NoteNumber, ev.note.Velocity})
		case noteEnd:
			result = append(result, midi.NoteOffEvent{deltaTime, ev.note.NoteNumber, ev.note.Velocity})
		}
	}

	return result
}

const (
	noteStart = iota
	noteEnd
)

type noteEvent struct {
	time float64
	kind int
	note Note
}
