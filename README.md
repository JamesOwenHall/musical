# Computed Music

## Project Information

The purpose of this project is to have an application generate song structures.  The application would be responsible for generating melodies for different instruments, genres and other parameters.  The sounds design would be left to the user.

The user runs the application and supplies the necessary parameters for the application.  The application then generates a MIDI file containing the musical information that can be read by a DAW.